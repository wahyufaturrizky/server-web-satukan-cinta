import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: ['debug', 'log', 'error', 'warn', 'verbose'],
  });
  await app.listen(process.env.PORT || 3001);
}
bootstrap();
