import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GraphQLModule } from '@nestjs/graphql';
import { UsersModule } from './users/users.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/models/user';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule.forRoot()],
      useFactory: async (configService: ConfigService) => ({
        type: 'mongodb' as 'mongodb',
        url: configService.get('MONGODB_URI'),
        database: configService.get('MONGODB_DATABASE'),
        // host: configService.get('HOST'),
        // port: configService.get('PORT'),
        // username: configService.get('USERNAME'),
        // password: configService.get('PASSWORD'),
        // entities: [__dirname + '/**/*.entity{.ts,.js}'],
        entities: [User],
        // synchronize: true,
        logging: ['query', 'error', 'schema', 'warn', 'info', 'log'] as ['query', 'error', 'schema', 'warn', 'info', 'log'],
      }),
      inject: [ConfigService],
    }),
    UsersModule,
    GraphQLModule.forRoot({
      installSubscriptionHandlers: false,
      autoSchemaFile: 'schema.gql',
    }),
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
