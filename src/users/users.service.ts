import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "./models/user";
import { Repository } from "typeorm";

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async findOne(id: number): Promise<User> {
    return await this.userRepository.findOne(id);
  }

  async findOneByCanonicalSlug(canonicalSlug: string): Promise<User> {
    const user = await this.userRepository.findOne({where: {canonicalSlug: canonicalSlug}});
    const transform = user.photoLocked ? 'sc_thumblock' : 'sc_thumb';
    const transformFull = user.photoLocked ? 'sc_fulllock' : 'sc_full';

    user.photoThumbnailUrl = user.photoV3 ? `${process.env.MEDIA_URL}/CACHE/${transform}/${user.photoV3}` : null;
    console.log('findOneByCanonicalSlug', canonicalSlug, ':', user);

    user.photoFullUrl = user.photoV3 ? `${process.env.MEDIA_URL}/CACHE/${transformFull}/${user.photoV3}` : null;
    console.log('findOneByCanonicalSlug', canonicalSlug, ':', user);
    return user;
  }

  async findAllLimited(): Promise<User[]> {
    return await this.userRepository.find({take: 10});
  }
}
