import { Resolver, Query, Args } from "@nestjs/graphql";
import { User } from "./models/user";
import { Int, ID } from "type-graphql";
import { Inject } from "@nestjs/common";
import { UsersService } from "./users.service";

@Resolver(of => User)
export class UsersResolver {
  constructor(
    private readonly usersService: UsersService,
  ) {}

  @Query(returns => User)
  async user(@Args({name: 'id', type: () => ID, nullable: true}) id: string,
    @Args({name: 'slug', type: () => String, nullable: true}) slug: string): Promise<User> {
    if (id != null) {
      return await this.usersService.findOne(parseInt(id));
    } else if (slug != null) {
      const canonicalSlug = slug.toLowerCase().replace(/[^a-z0-9]/g, '');
      return await this.usersService.findOneByCanonicalSlug(canonicalSlug);
    } else {
      throw new Error('one of id or slug is required');
    }
    // const user: User = {id: '123', createdAt: new Date(), updatedAt: new Date(), slug: 'hendy', name: 'Hendy Irawan'};
    // return user;
  }

  @Query(returns => [User])
  async findAllLimit(): Promise<User[]> {
    return await this.usersService.findAllLimited();
  }

}