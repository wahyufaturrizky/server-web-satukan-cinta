import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User, AccountStatus, MatchProfileStatus, Gender, Religion, SortDirection } from "./models/user";
import { Repository, FindManyOptions } from "typeorm";
import { CompactProfile } from "./models/compact-profile";
import { Logger } from '@nestjs/common';

@Injectable()
export class CompactProfilesService {
  private readonly logger = new Logger(CompactProfilesService.name);

  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async findAllCompactProfiles(): Promise<CompactProfile[]> {
    const users = await this.userRepository.find({
      where: {status: 'A', matchProfileStatus: 'A'},
      select: ['id', 'slug', 'updatedAt', 'photoV3', 'photoId', 'photoKey', 'photoLocked',
        'relationship', 'gender', 'age', 'addressCityDisplayName'],
      take: 10});
    const compactProfiles = users as CompactProfile[];
    compactProfiles.forEach(compactProfile => {
      const transform = compactProfile.photoLocked ? 'sc_thumblock' : 'sc_thumb';
      compactProfile.photoThumbnailUrl = compactProfile.photoV3 ? `${process.env.MEDIA_URL}/CACHE/${transform}/${compactProfile.photoV3}` : null;
    });
    // console.debug('findAllCompactProfiles() =>', compactProfiles);
    return users as CompactProfile[];
  }

  async findAllByStatusAndGenderAndReligionFamilyAndAge(
    matchProfileStatus?: MatchProfileStatus, gender?: Gender, religionFamily?: Religion,
    minAge?: number, maxAge?: number,
    sortDirection?: SortDirection, sortProperty?: string, offset?: number, limit?: number
  ): Promise<CompactProfile[]> {
    this.logger.debug(`findAllByStatusAndGenderAndReligionsAndAge(${matchProfileStatus}, ${gender}, ${religionFamily}, ${minAge}..${maxAge}, ${sortDirection}/${sortProperty}, ${offset}/${limit}) ...`);
    const where: any = {status: AccountStatus.A};
    if (matchProfileStatus) {
      where.matchProfileStatus = matchProfileStatus;
    }
    if (gender) {
      where.gender = gender;
    }
    if (religionFamily) {
      const religions = religionFamily == Religion.C ? [Religion.P, Religion.T] : [religionFamily];
      where.religion = {$in: religions};
    }
    if (minAge != null || maxAge != null) {
      where.age = {$gte: minAge != null ? minAge : undefined, $lte: maxAge != null ? maxAge : undefined};
    }
    
    const params: FindManyOptions<User> = {
      where: where,
      select: ['id', 'slug', 'updatedAt', 'photoV3', 'photoId', 'photoKey', 'photoLocked',
        'relationship', 'gender', 'age', 'addressCityDisplayName'],
      order: {[sortProperty]: sortDirection},
      skip: offset,
      take: limit};
    this.logger.debug(`findAllByStatusAndGenderAndReligionsAndAge ${params} ...`);
    const compactProfiles = (await this.userRepository.find(params)) as CompactProfile[];
    this.logger.log(`findAllByStatusAndGenderAndReligionsAndAge ${params}: ${compactProfiles.length} docs`);
    compactProfiles.forEach(compactProfile => {
      const transform = compactProfile.photoLocked ? 'sc_thumblock' : 'sc_thumb';
      compactProfile.photoThumbnailUrl = compactProfile.photoV3 ? `${process.env.MEDIA_URL}/CACHE/${transform}/${compactProfile.photoV3}` : null;
    });
    return compactProfiles;
  }
}
