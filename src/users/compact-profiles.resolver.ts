import { Resolver, Query, Args } from "@nestjs/graphql";
import { User, MatchProfileStatus, Gender, Religion, SortDirection, Relationship } from "./models/user";
import { Int, Arg } from "type-graphql";
import { CompactProfile, CompactProfileConnection, CompactProfileEdge } from "./models/compact-profile";
import { CompactProfilesService } from "./compact-profiles.service";

@Resolver(of => CompactProfile)
export class CompactProfilesResolver {
  constructor(
    private readonly compactProfilesService: CompactProfilesService,
  ) {}

  // @Query(returns => [CompactProfile])
  // async findAllCompactProfiles(): Promise<CompactProfile[]> {
  //   return await this.compactProfilesService.findAllCompactProfiles();
  // }

  @Query(() => CompactProfileConnection, 
    {description: 'List users with profiles using compact representation.'})
  async compactProfiles(
    @Args({name: 'matchProfileStatus', type: () => MatchProfileStatus, nullable: true})
    matchProfileStatus?: MatchProfileStatus, 
    @Args({name: 'gender', type: () => Gender, nullable: true})
    gender?: Gender,
    @Args({name: 'religionFamily', type: () => Religion, nullable: true, description: 'Religion family'})
    religionFamily?: Religion,
    @Args({name: 'relationships', type: () => [Relationship], nullable: true, description: 'Relationship status(es) criteria.'})
    relationships?: Relationship[],
    @Args({name: 'minAge', type: () => Int, nullable: true, description: 'Minimum age'})
    minAge?: number, 
    @Args({name: 'maxAge', type: () => Int, nullable: true, description: 'Maximum age'})
    maxAge?: number,
    @Args({name: 'sortDirection', type: () => SortDirection, nullable: true, description: 'Sort direction',
      defaultValue: SortDirection.DESC})
    sortDirection: SortDirection = SortDirection.DESC,
    @Args({name: 'sortProperty', type: () => String, nullable: true, description: 'Sort property',
      defaultValue: 'ctr'})
    sortProperty: string = 'ctr',
    @Args({name: 'after', type: () => String, nullable: true, description: 'After'})
    after?: string,
    @Args({name: 'first', type: () => Int, nullable: true, description: 'First'})
    first?: number,
    @Args({name: 'before', type: () => String, nullable: true, description: 'Before'})
    before?: string,
    @Args({name: 'last', type: () => Int, nullable: true, description: 'Last'})
    last?: number
  ): Promise<CompactProfileConnection> {
    const offset = after != null ? parseInt(after) + 1 : 0;
    const limit = first != null ? first : (last != null ? last : 20);
    const list = await this.compactProfilesService.findAllByStatusAndGenderAndReligionFamilyAndAge(
      matchProfileStatus, gender, religionFamily, minAge, maxAge,
      sortDirection, sortProperty, offset, limit + 1);
    const sliced = list.slice(0, limit);
    return {
      edges: sliced.map((it, index) => (<CompactProfileEdge> {cursor: (offset + index).toString(), node: it})),
      pageInfo: {
        hasNextPage: list.length > limit,
        hasPreviousPage: offset > 0,
        startCursor: sliced.length > 0 ? offset.toString() : null,
        endCursor: sliced.length > 0 ? (offset + limit - 1).toString() : null,
      }
    };
  }

}