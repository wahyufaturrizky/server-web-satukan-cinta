import { Module } from '@nestjs/common';
import { UsersResolver } from './users.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './models/user';
import { UsersService } from './users.service';
import { CompactProfilesService } from './compact-profiles.service';
import { CompactProfilesResolver } from './compact-profiles.resolver';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  providers: [UsersService, UsersResolver,
    CompactProfilesService, CompactProfilesResolver],
})
export class UsersModule {}
