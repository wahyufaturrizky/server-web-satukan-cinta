import { Field, ObjectType, Int, ID } from 'type-graphql';
import { PageInfo, Node } from './relay';
import { Gender, Relationship } from './user';

@ObjectType({implements: Node})
export class CompactProfile {

  @Field(type => ID, {nullable: false, description: 'ID (backed by a 32-bit integer).'})
  id: string;

  @Field({nullable: true, description: 'Unique username. lowercase, numbers and underscore; must start with letter.'})
  slug?: string;

  /**
   * Required for cursor pagination.
   */
  @Field({nullable: true, description: 'Updated at.'})
  updatedAt?: Date;

  @Field({nullable: true, description: 'Primary photo key for SatukanCinta v3, e.g. `person/profile/cari-jodoh-pria-hendy-143.jpg`.'})
  photoV3?: string;

  @Field({nullable: true,
    description: 'Primary photo ID, e.g. `aa2f42a1142a8c4ec8f47607`.'})
  photoId?: string;

  @Field({nullable: true,
    description: 'Primary photo key, e.g. `user/profile/aa/2f/42/a1/ade_satrio-aa2f42a1142a8c4ec8f47607.jpeg`.'})
  photoKey?: string;

  @Field({nullable: true,
    description: 'If `true`, photo is blurred and is only viewable by self or specified users.'})
  photoLocked?: boolean;

  @Field({nullable: true,
    description: 'Relationship status.'})
  relationship?: Relationship;

  @Field(() => Gender, {nullable: true, description: 'User\'s gender.'})
  gender?: Gender;

  @Field(() => Int, {nullable: true,
    description: 'User\'s age.'})
  age?: number;

  @Field({nullable: true,
    description: 'Address as human-friendly display name.'})
  addressCityDisplayName?: string;

  // Calculated fields

  @Field({nullable: true,
    description: 'Photo URL, ready to use.'})
  photoThumbnailUrl?: string;

}

@ObjectType()
export class CompactProfileEdge {
  @Field()
  cursor: string;
  @Field(type => CompactProfile)
  node?: CompactProfile;
}

@ObjectType()
export class CompactProfileConnection {
  @Field(type => [CompactProfileEdge])
  edges: CompactProfileEdge[];
  @Field()
  pageInfo: PageInfo;
}
