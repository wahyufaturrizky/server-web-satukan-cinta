import { Field, ID, ObjectType, Int, registerEnumType } from 'type-graphql';
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { Node } from './relay';

export enum Gender {
  /**
   * Unknown.
   */
  U = 'U',
  /**
   * Male.
   */
  M = 'M',
  /**
   * Female.
   */
  F = 'F',
}
registerEnumType(Gender, {name: 'Gender', description: 'Gender. U=Unknown, M=Male, F=Female.'});

export enum AccountStatus {
  /**
   * Draft.
   */
  D = 'D',
  /**
   * Inactive.
   */
  I = 'I',
  /**
   * Active.
   */
  A = 'A',
  /**
   * Suspended.
   */
  S = 'S',
}
registerEnumType(AccountStatus, {name: 'AccountStatus',
  description: 'Account status. D=Draft, I=Inactive, A=Active, S=Suspended.'});

export enum MatchProfileStatus {
  /**
   * Draft.
   */
  D = 'D',
  /**
   * Inactive.
   */
  I = 'I',
  /**
   * Active.
   */
  A = 'A',
  /**
   * Suspended.
   */
  S = 'S',
}
registerEnumType(MatchProfileStatus, {name: 'MatchProfileStatus',
  description: 'Match profile status. D=Draft, I=Inactive, A=Active, S=Suspended.'});

export enum HeightUnit {
  /**
   * cm.
   */
  cm = 'cm',
  /**
   * foot.
   */
  ft = 'ft',
}
registerEnumType(HeightUnit, {name: 'HeightUnit',
  description: 'Height unit.'});

export enum PreferredRelationship {
  /**
   * Polygyny.
   */
  P = 'P',
  /**
   * Available.
   */
  A = 'A',
  /**
   * Single.
   */
  V = 'V',
  /**
   * Friend.
   */
  F = 'F',
}
registerEnumType(PreferredRelationship, {name: 'PreferredRelationship',
  description: 'PreferredRelationship.'});

export enum Smoking  {
  /**
   * Never.
   */
  N = 'N',
  /**
   * Stopped.
   */
  D = 'D',
  /**
   * Sometimes.
   */
  S = 'S',
  /**
   * Regular.
   */
  R = 'R',
}
registerEnumType(Smoking, {name: 'Smoking',
  description: 'Smoking.'});

export enum Eyewear {
  /**
   * None.
   */
  N = 'N',
  /**
   * Glasses.
   */
  G = 'G',
  /**
   * Contact lenses.
   */
  C = 'C',
}
registerEnumType(Eyewear, {name: 'Eyewear',
  description: 'Eyewear.'});

export enum Piercing {
  /**
   * None.
   */
  N = 'N',
  /**
   * Public parts
   */
  U = 'U',
  /**
   * Private parts
   */
  V = 'V',
}
registerEnumType(Piercing, {name: 'Piercing', description: 'Piercing.'});

export enum Religion {
  /**
   * Islam.
   */
  I = 'I',
  /**
   * Christianity.
   */
  C = 'C',
  /**
   * Protestantism.
   */
  P = 'P',
  /**
   * Catholic church.
   */
  T = 'T',
}
registerEnumType(Religion, {name: 'Religion', description: 'Religion.'});

export enum PremiumStatus {
  /**
   * Unpaid
   */
  U = 'U',
  /**
   * Associate
   */
  A = 'A',
  /**
   * Plus
   */
  P = 'P',
  /**
   * Silver
   */
  S = 'S',
  /**
   * Gold
   */
  G = 'G',
  /**
   * Platinum
   */
  T = 'T',
}
registerEnumType(PremiumStatus, {name: 'PremiumStatus', description: 'PremiumStatus.'});

export enum currency {
  /**
   * Indonesian rupiah
   */
  IDR = 'IDR',
  /**
   * United States dollar
   */
  USD = 'USD',
  /**
   * Malaysian ringgit
   */
  MYR = 'MYR',
  /**
   * Brunei dollar
   */
  BND = 'BND',
  /**
   * Singapore dollar
   */
  SGD = 'SGD',
  /**
   * New Taiwan dollar
   */
  TWD = 'TWD',
  /**
   * Saudi riyal
   */
  SAR = 'SAR',
  /**
   * Canadian dollar
   */
  CAD = 'CAD',
  /**
   * Indian rupee
   */
  INR = 'INR',
  /**
   * Euro
   */
  EUR = 'EUR',
  /**
   * Australian dollar
   */
  AUD = 'AUD',
  /**
   * Hong Kong dollar
   */
  HKD = 'HKD',
  /**
   * British pound
   */
  GBP = 'GBP',
  /**
   * Russian ruble
   */
  RUB = 'RUB',
  /**
   * United Arab Emirates dirham
   */
  AED = 'AED',
  /**
   * South Korean won
   */
  KRW = 'KRW',
}
registerEnumType(currency, {name: 'currency', description: 'currency.'});

export enum SkinTone {
  /**
   * Black.
   */
  B = 'B',
  /**
   * Dark Brown.
   */
  D = 'D',
  /**
   * Light Brown.
   */
  R = 'R',
  /**
   * Light Yellow.
   */
  Y = 'Y',
  /**
   * White.
   */
  M = 'M',
}
registerEnumType(SkinTone, {name: 'SkinTone', description: 'SkinTone.'});

export enum HijabKind {
  /**
   * None.
   */
  N = 'N',
  /**
   * Hijab
   */
  H = 'H',
  /**
   * Sometimes
   */
  S = 'S',
}
registerEnumType(HijabKind, {name: 'HijabKind', description: 'HijabKind.'});

export enum Drinking {
  /**
   * Never.
   */
  N = 'N',
  /**
   * Stopped
   */
  D = 'D',
  /**
   * Sometimes
   */
  S = 'S',
  /**
   * Regular
   */
  R = 'R',
}
registerEnumType(Drinking, {name: 'Drinking', description: 'Drinking.'});

export enum Tattoo {
  /**
   * None.
   */
  N = 'N',
  /**
   * Facial
   */
  F = 'F',
  /**
   * Public parts
   */
  U = 'U',
  /**
   * Private parts
   */
  V = 'V',
}
registerEnumType(Tattoo, {name: 'Tattoo', description: 'Tattoo.'});

export enum OnlineStatus {
  /**
   * Unknown.
   */
  U = 'U',
  /**
   * Online
   */
  A = 'A',
  /**
   * Offline
   */
  I = 'I',
}
registerEnumType(OnlineStatus, {name: 'OnlineStatus', description: 'OnlineStatus.'});

export enum BodyKind {
  /**
   * Female slender (all thin).
   */
  S = 'S',
  /**
   * Female hourglass (big chest, bigger waist).
   */
  H = 'H',
  /**
   * Female pear (thin but muscly).
   */
  P = 'P',
  /**
   * Female well-endowed: big chest, small waist.
   */
  W = 'W',
  /**
   * Female petite: small chest, medium waist.
   */
  E = 'E',
  /**
   * Male inverted triangle (although can be used for female), but we don’t have matching silhouette sets.
   */
  I = 'I',
  /**
   * Male rhomboid
   */
  M = 'M',
  /**
   * Male rectangle
   */
  R = 'R',
  /**
   * Male triangle
   */
  T = 'T',
  /**
   * Male oval
   */
  O = 'O',
}
registerEnumType(BodyKind, {name: 'BodyKind', description: 'BodyKind.'});

export enum Relationship {
  /**
   * Single.
   */
  S = 'S',
  /**
   * Married.
   */
  M = 'M',
  /**
   * Divorced.
   */
  V = 'V',
  /**
   * Widowed.
   */
  W = 'W'
}
registerEnumType(Relationship, {name: 'Relationship', 
  description: 'Relationship status. S=Single, M=Married, V=Divorced, W=Widowed.'});

/**
 * Using TypeORM's naming.
 */ 
export enum SortDirection {
  ASC = 'ASC',
  DESC = 'DESC',
}
registerEnumType(SortDirection, {name: 'SortDirection', description: 'Sort direction.'});

/**
 * {
    "type": "Point",
    "coordinates": [125.6, 10.1]
  }
 */
@ObjectType({ description: "GeoJSON Point" })
class GeoPoint {
  @Field(type => String, {description: "GeoJSON Feature type, e.g. `Point`."})
  type: string;

  @Field(type => [Number], { description: "Coordinates, from x (longitude) and y (latitude)." })
  coordinates: number[];
}

@Entity({name: 'user'})
@ObjectType({implements: Node})
export class User {

  @PrimaryGeneratedColumn({name: '_id', type: 'int'})
  @Field(type => ID, {nullable: false, description: 'ID (backed by a 32-bit integer).'})
  id?: string;

  @Column()
  @Field({nullable: true, description: 'Unique username.'})
  slug?: string;

  @Column()
  @Field({nullable: true, description: 'Full name.'})
  name?: string;

  @Column()
  @Field({nullable: true, description: 'Created at.'})
  createdAt?: Date;

  @Column()
  @Field({nullable: true, description: 'Updated at.'})
  updatedAt?: Date;

  @Column()
  @Field({nullable: true, description: 'Primary photo key for SatukanCinta v3, e.g. `person/profile/cari-jodoh-pria-hendy-143.jpg`.'})
  photoV3?: string;

  @Field({nullable: true, description: 'Photo URL, ready to use.'})
  photoThumbnailUrl?: string;

  @Field({nullable: true, description: 'photoFullUrl.'})
  photoFullUrl?: string;

  @Column()
  @Field({nullable: true,
    description: 'Primary photo ID, e.g. `aa2f42a1142a8c4ec8f47607`.'})
  photoId?: string;

  @Column()
  @Field({nullable: true,
    description: 'Primary photo key, e.g. `user/profile/aa/2f/42/a1/ade_satrio-aa2f42a1142a8c4ec8f47607.jpeg`.'})
  photoKey?: string;

  @Column()
  @Field({nullable: true,
    description: 'If `true`, photo is blurred and is only viewable by self or specified users.'})
  photoLocked?: boolean;

  @Column()
  @Field(() => Relationship, {nullable: true,
    description: 'Relationship status.'})
  relationship?: Relationship;

  @Column()
  @Field(() => Gender, {nullable: true,
    description: 'User\'s gender.'})
  gender?: Gender;

  @Column()
  @Field(() => Int, {nullable: true,
    description: 'User\'s age.'})
  age?: number;

  @Column()
  @Field({nullable: true,
    description: 'Address as human-friendly display name.'})
  addressCityDisplayName?: string;

  @Column()
  @Field(() => AccountStatus, {nullable: true, description: 'Account status.'})
  status?: AccountStatus;

  @Column()
  @Field(() => MatchProfileStatus, {nullable: true, description: 'Match profile status.'})
  matchProfileStatus?: MatchProfileStatus;

  @Column()
  @Field({nullable: true, description: 'Not unique, someday we may want email-less user accounts.'})
  email?: string;

  @Column()
  @Field({nullable: true, description: 'addressCity.'})
  addressCity?: string;
  
  @Column()
  @Field({nullable: true, description: 'addressCountry.'})
  addressCountry?: string;

  @Column()
  @Field({nullable: true, description: 'headline.'})
  headline?: string;

  @Column()
  @Field({nullable: true, description: 'prefersGlassless.'})
  prefersGlassless?: boolean;

  @Column()
  @Field({nullable: true, description: 'prefersHijab.'})
  prefersHijab?: boolean;

  @Column()
  @Field({nullable: true, description: 'description.'})
  description?: string;

  @Column()
  @Field({nullable: true, description: 'preferredMinEduLevelId.'})
  preferredMinEduLevelId?: string;

  @Column()
  @Field({nullable: true, description: ' .'})
  weightKg?: number;

  @Column()
  @Field({nullable: true, description: 'preferredMinIncome.'})
  preferredMinIncome?: number;

  @Column()
  @Field(() => [String], {nullable: true, description: 'preferredCultures.'})
  preferredCultures?: string[];
  
  @Column()
  @Field({nullable: true, description: 'height.'})
  height?: number;
  
  @Column()
  @Field({nullable: true, description: 'income.'})
  income?: number;

  @Column()
  @Field({nullable: true, description: 'preferredMinAge.'})
  preferredMinAge?: number;

  @Column()
  @Field({nullable: true, description: 'preferredMaxAge.'})
  preferredMaxAge?: number;

  @Column()
  @Field({nullable: true, description: 'preferredMinHeight.'})
  preferredMinHeight?: number;

  @Column()
  @Field({nullable: true, description: 'preferredMaxHeight.'})
  preferredMaxHeight?: number;

  @Column()
  @Field({nullable: true, description: 'skinTone.'})
  skinTone?: SkinTone;

  @Column()
  @Field(() => PreferredRelationship, {nullable: true, description: 'PreferredRelationship.'})
  preferredRelationship?: PreferredRelationship;

  @Column()
  @Field(() => HeightUnit, {nullable: true, description: 'heightUnit.'})
  heightUnit?: HeightUnit;

  @Column()
  @Field(() => [SkinTone], {nullable: true, description: 'SkinTone.'})
  preferredSkinTones?: SkinTone[];

  @Column()
  @Field(() => Eyewear, {nullable: true, description: 'heightUnit.'})
  eyewear?: Eyewear;

  @Column()
  @Field(() => Smoking, {nullable: true, description: 'smoking.'})
  smoking?: Smoking;

  @Column()
  @Field(() => Religion, {nullable: true, description: 'religion.'})
  religion?: Religion;

  @Column()
  @Field({nullable: true, description: 'canonicalSlug.'})
  canonicalSlug?: string;

  @Column()
  @Field({nullable: true, description: 'emailVerifyCode.'})
  emailVerifyCode?: string;

  @Column()
  @Field({nullable: true, description: 'emailVerificationTime.'})
  emailVerificationTime?: Date;

  @Column()
  @Field({nullable: true, description: 'emailVerificationTime.'})
  emailBounceTime?: Date;

  @Column()
  @Field({nullable: true, description: 'emailBounceReason.'})
  emailBounceReason?: string;

  @Column()
  @Field({nullable: true, description: 'emailProbeTime.'})
  emailProbeTime?: Date;

  @Column()
  @Field({nullable: true, description: 'mobileNumber.'})
  mobileNumber?: string;
  
  @Column()
  @Field({nullable: true, description: 'mobileNumberPending.'})
  mobileNumberPending?: string;

  @Column()
  @Field({nullable: true, description: 'mobileVerifyCode.'})
  mobileVerifyCode?: string;

  @Column()
  @Field({nullable: true, description: 'mobileVerificationTime.'})
  mobileVerificationTime?: Date;

  @Column()
  @Field({nullable: true, description: 'mobileBounceTime.'})
  mobileBounceTime?: Date;

  @Column()
  @Field({nullable: true, description: 'mobileNumber2.'})
  mobileNumber2?: string;

  @Column()
  @Field({nullable: true, description: 'firstName.'})
  firstName?: string;

  @Column()
  @Field({nullable: true, description: 'lastName.'})
  lastName?: string;

  @Column()
  @Field({nullable: true, description: 'language.'})
  language?: string;

  @Column()
  @Field({nullable: true, description: 'locale.'})
  locale?: string;

  @Column()
  @Field({nullable: true, description: 'countryCode.'})
  countryCode?: string;

  @Column()
  @Field({nullable: true, description: 'birthDate.'})
  birthDate?: string;

  @Column()
  @Field(() => BodyKind, {nullable: true, description: 'bodyKind.'})
  bodyKind?: BodyKind;

  @Column()
  @Field({nullable: true, description: 'lastStatusUpdate.'})
  lastStatusUpdate?: string;

  @Column()
  @Field({nullable: true, description: 'childCount.'})
  childCount?: number;

  @Column()
  @Field(() => HijabKind, {nullable: true, description: 'hijabKind.'})
  hijabKind?: HijabKind;

  @Column()
  @Field(() => Drinking, {nullable: true, description: 'drinking.'})
  drinking?: Drinking;

  @Column()
  @Field(() => Tattoo, {nullable: true, description: 'tattoo.'})
  tattoo?: Tattoo;
  
  @Column()
  @Field(() => Piercing, {nullable: true, description: 'piercing.'})
  piercing?: Piercing;

  @Column()
  @Field({nullable: true, description: 'addressState.'})
  addressState?: string;

  @Column()
  @Field({nullable: true, description: 'addressCountryCode.'})
  addressCountryCode?: string;

  @Column()
  @Field(() => GeoPoint, {nullable: true, description: 'addressPoint.'})
  addressPoint?: GeoPoint;

  @Column()
  @Field({nullable: true, description: 'addressGeonameId.'})
  addressGeonameId?: number;

  @Column()
  @Field({nullable: true, description: 'addressCountryId.'})
  addressCountryId?: number;

  @Column()
  @Field({nullable: true, description: 'addressAdminId1.'})
  addressAdminId1?: number;

  @Column()
  @Field({nullable: true, description: 'addressAdminCode1.'})
  addressAdminCode1?: string;

  @Column()
  @Field({nullable: true, description: 'addressAdminId2.'})
  addressAdminId2?: number;

  @Column()
  @Field({nullable: true, description: 'addressAdminCode2.'})
  addressAdminCode2?: string;

  @Column()
  @Field(() => OnlineStatus, {nullable: true, description: 'Current online status (can be provided by Rocket.chat/chat engine).'})
  onlineStatus?: OnlineStatus;

  @Column()
  @Field({nullable: true, description: 'seenTime.'})
  seenTime?: Date;

  @Column()
  @Field(() => [String], {nullable: true, description: 'cultures.'})
  cultures?: string[];

  @Column()
  @Field(() => [String], {nullable: true, description: 'effCultures.'})
  effCultures?: string[];

  @Column()
  @Field({nullable: true, description: 'heightCm.'})
  heightCm?: number;

  @Column()
  @Field({nullable: true, description: 'IP address used during sign up.'})
  signupIpAddress?: string;

  @Column()
  @Field({nullable: true, description: 'IP address used during last sign in.'})
  lastIpAddress?: string;

  @Column()
  @Field({nullable: true, description: 'IP address used during last sign in.'})
  lastLoginTime?: Date;

  @Column()
  @Field({nullable: true, description: 'DEPRECATED.'})
  occupationLegacyId?: string;

  @Column()
  @Field({nullable: true, description: 'employmentId.'})
  employmentId?: string;

  @Column()
  @Field({nullable: true, description: 'employmentOther.'})
  employmentOther?: string;

  @Column()
  @Field({nullable: true, description: 'employmentOther.'})
  schoolId?: string;

  @Column()
  @Field({nullable: true, description: 'schoolOther.'})
  schoolOther?: string;

  @Column()
  @Field(() => [String], {nullable: true, description: 'Interests as array of CompactTopic objects.'})
  interests?: string[];

  @Column()
  @Field(() => [String], {nullable: true, description: 'Other interests that are not available in "topic" collection.'})
  interestOthers?: string[];

  @Column()
  @Field({nullable: true, description: 'preferredReligion.'})
  preferredReligion?: string;

  @Column()
  @Field({nullable: true, description: 'acceptsSmoking.'})
  acceptsSmoking?: Boolean;

  @Column()
  @Field({nullable: true, description: 'acceptsDrinking.'})
  acceptsDrinking?: Boolean;

  @Column()
  @Field({nullable: true, description: 'acceptsTattoo.'})
  acceptsTattoo?: Boolean;

  @Column()
  @Field({nullable: true, description: 'acceptsPiercing.'})
  acceptsPiercing?: Boolean;

  @Column()
  @Field({nullable: true, description: 'preference.'})
  preference?: string;

  @Column()
  @Field({nullable: true, description: 'featured.'})
  featured?: Boolean;

  @Column()
  @Field({nullable: true, description: 'eduLevelId.'})
  eduLevelId?: string;

  @Column()
  @Field({nullable: true, description: 'eduFieldId.'})
  eduFieldId?: string;

  @Column()
  @Field(() => currency, {nullable: true, description: 'Current online status (can be provided by Rocket.chat/chat engine).'})
  currency?: currency;

  @Column()
  @Field({nullable: true, description: 'impressionCount.'})
  impressionCount?: number;

  @Column()
  @Field({nullable: true, description: 'viewCount.'})
  viewCount?: number;

  @Column()
  @Field({nullable: true, description: 'ctr.'})
  ctr?: number;

  @Column()
  @Field({nullable: true, description: '(ERROR: duplicate key null). should be required.'})
  firebaseUid?: string;

  @Column()
  @Field({nullable: true, description: 'Int64 is not reliable in JavaScript, so we use String instead.'})
  facebookId?: string;

  @Column()
  @Field({nullable: true, description: 'facebookAccessToken'})
  facebookAccessToken?: string;

  @Column()
  @Field({nullable: true, description: 'facebookMe'})
  facebookMe?: string;

  @Column()
  @Field({nullable: true, description: 'googleAccessToken'})
  googleAccessToken?: string;

  @Column()
  @Field({nullable: true, description: 'signupPage'})
  signupPage?: string;

  @Column()
  @Field({nullable: true, description: 'suspendTime'})
  suspendTime?: Date;

  @Column()
  @Field({nullable: true, description: 'suspendReason'})
  suspendReason?: string;

  @Column()
  @Field({nullable: true, description: 'inactiveTime'})
  inactiveTime?: string;

  @Column()
  @Field(() => PremiumStatus, {nullable: true, description: 'PremiumStatus'})
  premium?: PremiumStatus;

  @Column()
  @Field({nullable: true, description: 'Payment time'})
  premiumTime?: Date;

  @Column()
  @Field({nullable: true, description: 'coachingPeriod'})
  coachingPeriod?: string;

  @Column()
  @Field({nullable: true, description: 'After this date, coaching plan (Silver/Gold/Platinum) will expire.'})
  coachingExpiresAt?: Date;

  @Column()
  @Field({nullable: true, description: 'Currency for `premiumAmount`'})
  premiumCurrency?: string;

  @Column()
  @Field({nullable: true, description: 'Amount paid in currency of `premiumCurrency`'})
  premiumAmount?: number;

  @Column()
  @Field({nullable: true, description: 'Amount actually received as our revenue (excluding processing fees)'})
  premiumNetAmount?: number;

  @Column()
  @Field({nullable: true, description: 'Last Xendit invoice ID (not guaranteed to be valid). Its external ID *must* have a format of `heartenly/{userId}/{premium}/{coachingPeriod}` that will be parsed upon payment.'})
  xenditInvoiceId?: string;

  @Column()
  @Field({nullable: true, description: 'Last Xendit invoice URL, can be staging or production.'})
  xenditInvoiceUrl?: string;

  @Column()
  @Field({nullable: true, description: 'Last Xendit invoice (JSON).'})
  xenditInvoiceJson?: string;

  @Column()
  @Field({nullable: true, description: 'Last Xendit invoice expiration time, we set validity as 30 days after invoice creation. If user clicks pay again after (expiration minus 12 hours), we"ll generate a new invoice.'})
  xenditExpiresAt?: Date;

  @Column()
  @Field({nullable: true, description: 'DEPRECATED'})
  idLegacy?: string;

  @Column()
  @Field({nullable: true, description: 'Cover photo ID, e.g. `ab000000142a8c4ec8f47604`'})
  coverPhotoId?: string;

  @Column()
  @Field({nullable: true, description: 'Cover photo key, e.g. `user/cover/ab/00/00/00/dvok_-ab000000142a8c4ec8f47604.jpeg`'})
  coverPhotoKey?: string;

  @Column()
  @Field(() => [String], {nullable: true, description: 'profileVisibility'})
  profileVisibility?: string[];

  @Column()
  @Field({nullable: true, description: 'If user consents to WhatsApp marketing, the timestamp of user`s consent. If user does not consent, null.'})
  whatsappConsentTime?: Date;

  @Column()
  @Field({nullable: true, description: 'If user consents to email marketing, the timestamp of user`s consent. If user does not consent, null.'})
  emailMarketingConsentTime?: Date;





}
