import { InterfaceType, Field, ObjectType, ID } from "type-graphql";

@InterfaceType()
export class Node {
  @Field(type => ID, {nullable: false, description: 'Opaque ID.'})
  id?: string;
}

@ObjectType()
export class PageInfo {
  @Field({nullable: false})
  hasNextPage: boolean;
  @Field({nullable: false})
  hasPreviousPage: boolean;
  @Field({nullable: true})
  startCursor?: string;
  @Field({nullable: true})
  endCursor?: string;
}
